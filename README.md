# Wizard

Modal wizard engine for Backbone.Marionette based upon
[overlay.js](https://bitbucket.org/wizzbuy/overlay).


## Features

- See [overlay.js](https://bitbucket.org/wizzbuy/overlay)
- Atomic view change
- Unobtrusive
- Evented


## Usages

### 1. One-liners

This invocation pattern is the less intrusive. It takes a standard view (or a
jQuery DOM built for that purpose) and a couple of parameters to present the
user with a fully featured wizard, complete with events.

```javascript
Confirmation = Marionette.View( /* ... */ );

var view = new Confirmation();

var wizard = new Marionette.Wizard()
  .show(view, {
    title: 'Confirmation required',
    buttons: [
      ['Cancel', 'dismiss', 'btn-default'],
      ['OK',     'accept',  'btn-primary']
    ]
  });

wizard.on('dismiss', function (origin) {
  // Called when the backdrop, the Close or the Cancel button are clicked. The
  // value of `origin` will be set to `backdrop` if appropriate.
});

wizard.on('accept', function () {
  // Called when the OK button is clicked.
});

wizard.on('close', function () {
  // Always called.
});
```

### 2. Process-oriented

This invocation pattern allows chaining views in a single overlay while
updating all its attributes atomically and handling views life-cycle. It's
farily easy to build complex interactions with this building block.

```javascript
Screen1 = Marionette.View( /* ... */ );
Screen2 = Marionette.View( /* ... */ );

Registration = Marionette.Wizard.extend({
  start: function () {
    this.show_screen_1();
  },

  show_screen_1: function () {
    this.show(new Screen1(), {
      title: 'Screen 1',
      buttons: [
        ['Next', 'next', 'btn-next']
      ]
    });
  },

  click_next: function () {
    this.show_screen_2();
  },

  show_screen_2: function () {
    this.show(new Screen2(), {
      close: false,
      title: 'Screen 2',
      buttons: [
        ['Done!', 'done', 'btn-primary']
      ]
    });
  },

  click_done: function () {
    // Send information to backend...

    this.close();
  }
});

new Registration()
  .start()
```

### 2. Service-oriented

This approach takes advantage of the events handling capabilities to deliver a
result to calling code that is fully agnostic of the details or their possible
asynchronous resolution.

```javascript
ImagePicker = {};

ImagePicker.View = Marionette.View.extend({
  // Standard view code here. There should be elements with
  // the attribute `data-action="pick"` in the template to trigger the
  // `click_pick` method.
});

ImagePicker.Overlay = Marionette.Wizard.extend({
  start: function (url) {
    var self = this;

    $.post('/api/v1/scraper', { url: url })
      .then(function (res) {
        var view = new ImagePicker.View({
          collection: new Backbone.Collection(res.images)
        });

        self.show(view);
      });
  },

  click_pick: function (e, $el) {
    var picture = new Backbone.Model({
      url: $el.data('url')
    });

    this.close();

    this.trigger('pick', picture);
  }
});

new ImagePicker.Overlay()
  .start('http://modizy.com')
  .on('pick', function (picture) {
    // Do something useful with `picture`.
  });
```


## Options

The following options are supported:

- `callback`: A callback function called when the wizard is show, closed and
  hidden.


## License

ISC © 2015 Acute Analytics / Benoit Myard <bmyard@acute.fr>
