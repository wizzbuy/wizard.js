Backbone.Marionette.Wizard = Backbone.Marionette.View.extend({

  className: 'wizard-Wizard',

  template: [
    '<div class="wizard-Wrapper">',
      '<% if (title) { %>',
        '<div class="wizard-Title">',
          '<h1><%- title %></h1>',
        '</div>',
      '<% } %>',

      '<div class="wizard-Body"></div>',

      '<% if (!_.isEmpty(buttons)) { %>',
        '<div class="wizard-Buttons">',
          '<div>',
            '<% _.each(buttons, function (button) { %>',
              '<button class="<%= classes %> <%= button.slice(2) %>"',
                'data-action="<%= button[1] %>">',
                '<%- button[0] %>',
              '</button>',
            '<% }); %>',
          '</div>',
        '</div>',
      '<% } %>',

      '<% if (close) { %>',
        '<div class="wizard-Close">',
          '<button data-action="dismiss"></button>',
        '</div>',
      '<% } %>',
    '</div>'
  ].join('\n'),

  defaults: {
    close: true,
    title: false,
    classes: '',
    buttons: []
  },

  events: {
    'click [data-action]': 'onAction'
  },

  constructor: function (options) {
    var self = this;

    this.options = options || {};

    var res = Backbone.Marionette.View.prototype.constructor.apply(this,
        arguments);

    if (this.name || this.options.name) {
      this.$el.addClass(this.name || this.options.name);
    }

    this.$el.one('closed', function (e, origin) {
      if (origin === 'backdrop') {
        self.click_dismiss(origin);
      }
    });

    return res;
  },

  onAction: function (e) {
    var $el = $(e.currentTarget);

    if ($el.is('a')) {
      e.preventDefault();
    }

    var action = $el.data('action');

    if (action) {
      if (typeof this['click_' + action] !== 'function') {
        throw new Error('Undefined click handler: ' + action);
      }

      this['click_' + action](e, $el);
    }
  },

  serializeData: function (data) {
    return _.defaults({}, data || {}, this.defaults);
  },

  getTemplate: function () {
    if (typeof this.template !== 'function') {
      this.template = _.template(this.template);
    }

    return this.template;
  },

  render: function () {
    this.triggerMethod('before:render', this);

    this.bindUIElements();

    this.triggerMethod('render', this);
  },

  show: function (view, options) {
    Marionette.triggerMethod.call(this, 'before:show', view);
    if (view instanceof Backbone.View) {
      Marionette.triggerMethod.call(view, 'before:show');
    }

    var template = this.getTemplate();
    var data = this.serializeData(options);

    var html = template(data);

    var el = this.openView(view);

    html = $(html);

    html.find('.wizard-Body').append(el);

    this.$el.empty();
    this.$el.append(html);

    if (this.currentView) {
      this.destroyView(this.currentView);
      this.currentView = null;
    }

    this.currentView = view;

    Marionette.triggerMethod.call(this, 'show', view);
    if (view instanceof Backbone.View) {
      Marionette.triggerMethod.call(view, 'show');
    }

    this.overlay();
    this.overlay('position');

    return this;
  },

  destroy: function () {
    if (this.isDestroyed) {
      return;
    }

    if (this.currentView) {
      this.destroyView(this.currentView);
      this.currentView = null;
    }

    Marionette.triggerMethod.call(this, 'before:destroy');

    this.overlay('close');

    Marionette.triggerMethod.call(this, 'destroy');

    this.isClosed = true;
  },

  openView: function (view) {
    if (typeof view === 'string') {
      return $(view).clone();
    }
    else if (view instanceof Backbone.View) {
      // Automatically render Marionette layouts.
      if (view.isClosed || view._firstRender || !view._isRendered) {
        view.render();
      }

      return view.el;
    }
    else {
      return view;
    }
  },

  destroyView: function (view) {
    if (view.isDestroyed) {
      return;
    }

    if (view.destroy) {
      view.destroy();
    }
    else if (view.remove) {
      view.remove();
    }
  },

  overlay: function () {
    this.$el.overlay.apply(this.$el, arguments);
  },

  click_dismiss: function (origin) {
    Marionette.triggerMethod.call(this, 'before:dismiss', origin);

    this.destroy();

    Marionette.triggerMethod.call(this, 'dismiss', origin);
  },

  click_accept: function () {
    Marionette.triggerMethod.call(this, 'before:accept');

    this.destroy();

    Marionette.triggerMethod.call(this, 'accept');
  }

});
